#include <stdio.h>
#include <math.h>
#include <windows.h>
#include <time.h>
#define VALUE_MAX 100 


unsigned long rnd = 1;
int Random_num()
{
	rnd = rnd * 14807;
	return((unsigned int)(rnd / 65536) % VALUE_MAX);
}

short Check_u(unsigned int array[], unsigned int num)
{
	short tmp = 0;
	for (int i = 0; i < 50000; i++)
	{
		if (array[i] == num)
		{
			tmp++;
		}
	}
	return tmp;
}

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	unsigned int a = 214013, c = 2531011, len = 50000;
	unsigned int m = (pow(2, 32));
	unsigned int* values;
	values = (unsigned int*)malloc(sizeof(unsigned int) * len);
	for (int i = 0; i < len; i++)
	{
		values[i] = Random_num();
	}

	short frequency[50000];

	printf("������� ��������� ���������� �����:\n");

	for (int i = 0; i < len; i++)
	{
		frequency[i] = Check_u(values, values[i]);
		printf("�������: �%d = %d\n", i, values[i]);
	}

	double statistic[50000];

	printf("����������� ���������� ����� ���������� �������:\n");

	for (int i = 0; i < len; i++) {
		statistic[i] = ((double)frequency[i] / len);
		printf("�������: %d = %0.4f\n", i, statistic[i]);
	}

	double mathExp = 0;

	for (int i = 0; i < len; i++)
	{
		mathExp += i * statistic[i];
	}

	printf("\n����������� ��������� ���������� �������: %f", mathExp);

	double dysp = 0;
	for (int i = 0; i < len; i++)
	{
		dysp += pow((i - mathExp), 2) * statistic[i];
	}
	printf("\n�������� ���������� �������: %f", dysp);
	printf("\n������������������� ��������� ���������� �������: %f\n", sqrt(dysp));
	system("pause");
}
