#include <iostream>
#include <Windows.h>
#include <time.h>
#include <malloc.h>
#include <stdlib.h>
#define MAX_N 10000

typedef int elemtype; // ��� �������� ������

struct elem {
    elemtype value; // �������� ����������
    struct elem* next; // ������ �� ��������� ������� ������
    struct elem* prev; // ������ �� ���������� ������� ������
};

struct myList {
    struct elem* head; // ������ ������� ������
    struct elem* tail; // ��������� ������� ������
    int size; // ���������� ��������� � ������
};

typedef struct elem cNode;
typedef struct myList cList;

cList* createList(void) { // ��������� ���������� ������
    cList* list = (cList*)malloc(sizeof(cList));
    if (list) {
        list->size = 0;
        list->head = list->tail = NULL;
    }
    return list;
}
bool isEmptyList(cList* list) // �������� �� ���������
{
    return ((list->head == NULL) || (list->tail == NULL));
}


int pushBack(cList* list, elemtype* data) {
    cNode* node = (cNode*)malloc(sizeof(cNode));
    if (!node) {
        return(-3);
    }
    node->value = *data;
    node->next = NULL;
    node->prev = list->tail;
    if (!isEmptyList(list)) {
        list->tail->next = node;
    }
    else {
        list->head = node;
    }
    list->tail = node;
    list->size++;
    return(0);
}

void printList(cList* list, void (*func)(elemtype*)) {
    cNode* node = list->head;
    if (isEmptyList(list)) {
        return;
    }
    while (node) {
        func(&node->value);
        node = node->next;
    }
}

void printInt(elemtype* value) {
    printf("%d ", *((int*)value));
}

cNode* getNode(cList* list, int index) {
    cNode* node = NULL;
    int i;
    if (index >= list->size) {
        return (NULL);
    }
    if (index < list->size / 2) {
        i = 0;
        node = list->head;
        while (node && i < index) {
            node = node->next;
            i++;
        }
    }
    else {
        i = list->size - 1;
        node = list->tail;
        while (node && i > index) {
            node = node->prev;
            i--;
        }
    }
    return node;
}


int main()
{
    myList* list = createList();
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    srand(time(NULL));
    int kil, a, arr[MAX_N];
    int c = -1000, b = 1000;
    printf("ʳ������ �������� ������: "); scanf_s("%d", &kil);

    printf("���������� ������� : \n");
    clock_t start, finish;
    start = clock();

    for (int i = 0; i < kil; i++) {
        arr[i] = c + rand() % (b - c + 1);
    }
    int imin;
    for (int i = 0; i < kil - 1; i++)
    {
        imin = i;
        for (int j = i + 1; j < kil; j++)
            if (arr[j] < arr[imin]) imin = j;
        a = arr[i];
        arr[i] = arr[imin];
        arr[imin] = a;
    }

    finish = clock();
    double duration1 = (double)(finish - start) / CLOCKS_PER_SEC;
    printf("\n��� ���������: %0.3f\n", duration1);



    printf("\n���������� ��������� : \n");
    clock_t start_2, finish_2;
    start_2 = clock();

    for (int i = 0; i < kil; i++) {
        arr[i] = c + rand() % (b - c + 1);
    }
    int cc;
    for (int i = 1; i < kil; i++)
    {
        cc = arr[i];
        for (int j = i - 1; j >= 0 && arr[j] > cc; j--)
        {
            arr[j + 1] = arr[j];
            arr[j] = cc;
        }
    }

    finish_2 = clock();
    double duration2 = (double)(finish_2 - start_2) / CLOCKS_PER_SEC;
    printf("\n��� ���������: %0.3f\n", duration2);

}
