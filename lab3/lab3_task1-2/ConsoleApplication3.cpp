#include <iostream>
#include <chrono>
#include <windows.h>
#include <math.h>

unsigned long g = 1;
int funct1(int l, double function[], double lnt);
int funct2(int l, double function[], double lnt);
int funct3(int l, double function[], double lnt);
int funct4(int l, double function[], double lnt);
int funct5(int l, double function[], double lnt);
int funct6(int l, double function[], double lnt);
int fact(int l);
void stup(int a, int b);
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int vubir;
	int l = 0;
	int a = 17, b =7;
	for (int j = 0; ; j++)
	{
		printf("������� ��������:\n");
		printf(" 1 - �������� 1.\n 2 - �������� 2.\n");
		scanf_s("%d", &vubir);
		if (vubir == 0)
		{
			break;
		}
		switch (vubir)
		{
		case 1:
			for (int i = 0; ; i++)
			{
				printf("������� ���� �������, �� ������� �����\n");
				printf(" 1 - f(n) = (n)\n 2 - f(n) = log(n)\n 3 - f(n) = n *log(n)\n 4 - f(n) = n ^ 2\n 5 - f(n) = 2 ^ n\n 6 - f(n) = n!\n 0 - ������� ��������!!!\n ���� :");
					scanf_s("%d", &vubir);
				if (vubir == 0)
				{
					break;
				}
				double function[51];
				int lnt = sizeof(function) / sizeof(double);
				switch (vubir)
				{
				case 1:
					printf("������� f(n) = (n)\n");
					funct1(l, function, lnt);
					break;
				case 2:
					printf("������� f(n) = log(n)\n");
					funct2(l, function, lnt);
					break;
				case 3:
					printf("������� f(n) = n * log(n)\n");
					funct3(l,function,lnt);
					break;
				case 4:
					printf("������� f(n) = n ^ 2\n");
					funct4(l, function, lnt);
					break;
				case 5:
					printf("������� f(n) = 2 ^ n\n");
					funct5(l, function, lnt);
					break;
				case 6:
					printf("������� f(n) = n!\n");
					funct6(l, function, lnt);
					break;
				default:
					printf("�� �������� � ���������� �������||��������� �����\n");
				}
			}
			break;
		case 2:
			vubir = 1;
			switch (vubir)
			{
			case 1:
				printf("������ � �� � ����� ����� \n0<=a<=20; 0<=b<=20\n");
				scanf_s("%d %d", &a, &b);
				 auto begin = std::chrono::steady_clock::now();
				 stup(a, b);
				 auto end = std::chrono::steady_clock::now();
				 auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin);
				 printf("��� ��������� �������: %lld\n", elapsed_ms.count());
				break;
			}
			break;
		default:
			printf("�� �������� �������||��������� �����\n");
			break;
		}
	}
}
int funct1(int l, double function[], double lnt)
{
	if (l == lnt)
	{
		return 1;
	}
	else
	{
		function[l] = l;
		printf("f(%d) = %0.0f\n", l, function[l]);
		return funct1(l + 1, function, lnt);
	}
}
int funct2(int l, double function[], double lnt)
{
	if (l == lnt)
	{
		return 1;
	}
	else
	{
		function[l] = log(l);
		printf("f(%d) = %0.2f\n", l, function[l]);
		return funct2(l + 1, function, lnt);
	}
}
int funct3(int l, double function[], double lnt)
{
	if (l == lnt)
	{
		return 1;
	}
	else
	{
		function[l] = log(l) + l;
		printf("f(%d) = %0.2f\n", l, function[l]);
		return funct3(l + 1, function, lnt);
	}
}
int funct4(int l, double function[], double lnt)
{
	if (l == lnt)
	{
		return 1;
	}
	else
	{
		function[l] = l * l;
		printf("f(%d) = %0.0f\n", l, function[l]);
		return funct4(l + 1, function, lnt);
	}
}
int funct5(int l, double function[], double lnt)
{
	if (l == lnt)
	{
		return 1;
	}
	else
	{
		function[l] = pow(2, l);
		printf("f(%d) = %0.0f\n", l, function[l]);
		return funct5(l + 1, function, lnt);
	}
}
int funct6(int l, double function[], double lnt)
{
	if (l == lnt)
	{
		return 1;
	}
	else
	{
		if (l == 0)
		{
			return funct6(l + 1, function, lnt);
		}
		function[l] = fact(l);
		printf("f(%d) = %0.0f\n", l, function[l]);
		return funct6(l + 1, function, lnt);
	}
}
int fact(int l)
{
	int answr = 1;
	if (l == 1)
	{
		return 1;
	}
	answr = fact(l - 1) * l;
	return answr;
}

void stup(int a, int b) {
	double m = pow(a,b);
	if (a >= 0 && a <= 20 && b <= 20 && b >= 0)
	{
		printf("  %f\n", m);
	}
	else 
	{
		printf("������� ������ �������� � �");
	}
}
