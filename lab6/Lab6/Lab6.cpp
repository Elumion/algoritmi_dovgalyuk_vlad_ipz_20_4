#include <stdio.h>
#include <Windows.h>
#include <time.h>
#include <malloc.h>
void GenArrPiram(float* arr, int n, int a, int b)
{
	for (int i = 0; i < n; i++)
	{
		arr[i] = a * rand() % (b - a + 1);
	}
}
void GenArrShell(double* arr, int n, int a, int b)
{
	for (int i = 0; i < n; i++)
	{
		arr[i] = a + rand() % (b - a + 1);
	}
}
void GenArrCout(short* arr, int n, int a, int b)
{
	for (int i = 0; i < n; i++)
	{
		arr[i] = a + rand() % (b - a + 1);
	}
}
void PrintArrPiram(float* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf(" %0.1f", arr[i]);
	}
	printf("\n");
}
void PrintArrShell(double* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf(" %d", arr[i]);
	}
	printf("\n");
}
void PrintArrCout(short* arr, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf(" %d", arr[i]);
	}
	printf("\n");
}
int BuildPyramid(float* arr, int i, int n)
{
	int imax;//��������� �������
	float tmp;
	if ((2 * i + 2) < n)
	{
		if (arr[2 * i + 1] < arr[2 * i + 2]) imax = 2 * i + 2;
		else imax = 2 * i + 1;
	}
	else imax = 2 * i + 1;
	if (imax >= n) return i;
	if (arr[i] < arr[imax])//�������� ��������
	{
		tmp = arr[i]; 
		arr[i] = arr[imax];
		arr[imax] = tmp;
		if (imax < n / 2) i = imax;
	}
	return i;
}
void SortPyramid(float* arr, int n)
{
	for (int i = n / 2 - 1; i >= 0; --i)//������ ������
	{
		long prevelem = i;
		i = BuildPyramid(arr, i, n);
		if (prevelem != i) ++i;
	}
	float tmp;
	for (int k = n - 1; k > 0; --k)//������� ������
	{
		tmp = arr[0];
		arr[0] = arr[k];
		arr[k] = tmp;
		int i = 0, prevelem = -1;
		while (i != prevelem)
		{
			prevelem = i;
			i = BuildPyramid(arr, i, k);
		}
	}
}
void SortShella(double* arr, double n)
{
	int s = 1;
	double tmp;
	int left, right;
	while (s <= n / 3)
	s = (3 * s - 1); s = s / 2;
	//�������� �������
	for (s; s > 0; s = ((s + 1) / 2) / 3)
		for (right = s; right < n; right++)
		{
			tmp = arr[right];
			for (left = right; left >= s; left -= s)
			{
				if (tmp < arr[left - s])
					arr[left] = arr[left - s];
				else
					break;
			}
			arr[left] = tmp;
		}
}
void SimpleSortCout(short* arr, int n)
{
	short min = arr[0];
	short max = arr[0];
	for (int i = 0; i < n; i++)//��������� ��������� �������
	{
		if (arr[i] > max)
		{
			max = arr[i];
		}
		else if (arr[i] < min)
		{
			min = arr[i];
		}
	}
		int length;
	int correct = min != 0 ? -min : 0;//�������� �� �� ����� -
	max += correct;
	length = correct + 1;
	int* arrcout;
	arrcout = (int*)calloc(length, sizeof(int));//��������� ����� �� ��������� ��������� ��������(���� ����� - �� ������� �� - 1)
		for (int i = 0; i < n; i++)
			//�������� �� ������ ����������� �������� ������� ������
			//(��������� ����� 30 ����� �� ������ 30 � 
			//���� ���� ���������� 1 (���� ����� ���� � ������� �� ������ ����������)
		{
			arrcout[arr[i] + correct]++;
		}
	int index = 0;
	for (int i = 0; i < length; i++)//���������� �����(������� ����)
	{
		for (int j = 0; j < arrcout[i]; j++)
		{
			arr[index] = i - correct;
			index++;
		}
	}
}
int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(NULL));
	int n;
	float arr[10000];
	double arr1[10000];
	short arr2[10000];
	double rez;
	int a = 20, b = 200;
	printf("������ ����� ������ ��� ϳ����������� ����������, ������� ����� � ����������� ϳ��������\n");
	scanf_s("%d", &n);
	printf("\nϳ��������� ����������:\n");
	clock_t start, finish;
	start = clock();
	GenArrPiram(arr, n, a, b);
	printf("\n���������� �����:\n");
	PrintArrPiram(arr, n);
	printf("\n³����������� ����� �� ��������� ������������ ����������:\n");
	SortPyramid(arr, n);
	PrintArrPiram(arr, n);
	finish = clock();
	rez = (double)(finish - start) / CLOCKS_PER_SEC;
	printf("\n��� ���������: %2f\n", rez);
	printf("\n------------------------------------------------------------------------------------------------------------------\n");
	printf("\n���������� ������� �����:\n");
	a = -10; b = 100;
	start = clock();
	GenArrShell(arr1, n, a, b);
	printf("\n���������� �����:\n");
	PrintArrShell(arr1, n);
	printf("\n³����������� ����� ������� �����:\n");
	SortShella(arr1, n);
	PrintArrShell(arr1, n);
	finish = clock();
	rez = (double)(finish - start) / CLOCKS_PER_SEC;
	printf("\n��� ���������: %2f\n", rez);
	printf("\n--------------------------------------------------------------------------------------------------------------\n");
	printf("\n���������� ������� ���������:\n");
	a = -10; b = 25;
	start = clock();
	GenArrCout(arr2, n, a, b);
	printf("\n���������� �����:\n");
	PrintArrCout(arr2, n);
	SimpleSortCout(arr2, n);
	printf("\n³����������� ����� ������� ���������:\n");
	PrintArrCout(arr2, n);
	finish = clock();
	rez = (double)(finish - start) / CLOCKS_PER_SEC;
	printf("\n��� ���������: %2f\n", rez);
	printf("\n------------------------------------------------------------------------------------------------------------------\n");
}