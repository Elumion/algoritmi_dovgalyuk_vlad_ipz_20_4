﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace once_list
{
	class Program
	{
		public class Node<T>
		{
			public Node(T data)
			{
				Data = data;
			}

			public T Data { set; get; }
			public Node<T> Next { set; get; }
		}

		public class LinkedList<T> : IEnumerable<T>
		{
			Node<T> head;
			Node<T> tail;
			int count;

			public void Add(T data)
			{
				Node<T> node = new Node<T>(data);

				if (head == null)
					head = node;
				else
					tail.Next = node;
				tail = node;

				count++;
			}

			public bool Remove(T data)
			{
				Node<T> current = head;
				Node<T> previous = null;

				while (current != null)
				{
					if (current.Data.Equals(data))
					{
						if (previous != null)
						{
							previous.Next = current.Next;

							if (current.Next == null)
								tail = previous;
						}

						else
						{
							head = head.Next;

							if (head == null)
								tail = null;
						}
						count--;
						return true;
					}

					previous = current;
					current = current.Next;
				}
				return false;
			}

			public void Clear()
			{
				head = null;
				tail = null;
				count = 0;
			}

			IEnumerator IEnumerable.GetEnumerator()
			{
				return ((IEnumerable)this).GetEnumerator();
			}

			IEnumerator<T> IEnumerable<T>.GetEnumerator()
			{
				Node<T> current = head;

				while (current != null)
				{
					yield return current.Data;
					current = current.Next;
				}
			}
		}
		static void Main(string[] args)
		{
			Console.OutputEncoding = Encoding.Unicode;
			int tmp;
			bool check;
			LinkedList<string> lst = new LinkedList<string>();

			Console.WriteLine("Реалізація завдання з однозв'язним списком:");
			do
			{
				Console.Write("Введіть кількість елементів для додавання в список: ");
				if (int.TryParse(Console.ReadLine(), out tmp))
					check = false;
				else
					check = true;

			} while (check);

			for (int i = 0; i < tmp; i++)
			{
				Console.Write("Введіть элемент: ");
				lst.Add(Console.ReadLine());
			}

			foreach (var item in lst)
			{
				Console.WriteLine(item);
			}

			while (true)
			{
				Console.WriteLine("Основні функції з однозв'язним списком:\n1. Додати новий елемент\n2. Видалити\n3. Очистити список");
				int task = Convert.ToInt32(Console.ReadLine());

				if (task == 1)
				{
					Console.Write("Введіть елемент: ");
					lst.Add(Console.ReadLine());

					foreach (var item in lst)
					{
						Console.WriteLine(item);
					}
				}
				else if (task == 2)
				{
					Console.Write("Введіть елемент для видалення: ");
					lst.Remove(Console.ReadLine());

					foreach (var item in lst)
					{
						Console.WriteLine(item);
					}
				}
				else if (task == 3)
					lst.Clear();
				else
					Console.WriteLine("Error!!!");
			}
		}
	}
}
